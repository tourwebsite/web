import React from 'react'
import './SearchStyles.css'

function Search() {
    return (
        <div name='book' className='search'>
            <div className="container">
                <div className="left">
                    <h2>LUXURY INCLUDED VACATIONS FOR TWO PEOPLE</h2>
                    <p>Come experience the very pinnacle of luxury Caribbean all-inclusive vacations.</p>
                    <div></div>
                </div>
            </div>
        </div>
    )
}

export default Search
