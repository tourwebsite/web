import React, { useState } from 'react'
import { FaBars, FaUser } from 'react-icons/fa'
import { AiOutlineClose } from 'react-icons/ai'
import { FaFacebook, FaInstagram, FaPinterest, FaTwitter, FaYoutube } from 'react-icons/fa'

import { Link } from 'react-scroll'

import './NavbarStyles.css'

function Navbar() {
    const [nav, setNav] = useState(false)
    const handleNav = () => setNav(!nav)

    return (
        <div name='home' className={nav ? 'navbar navbar-bg' : 'navbar'}>
            <div className={nav ? 'logo dark' : 'logo'}>
                <h2>Fractal City Tour</h2>
            </div>
            <ul className="nav-menu">
                <Link to='หน้าหลัก' smooth={true} duration={500} ><li>หน้าหลัก</li></Link>
                <Link to='หมวดหมู่' smooth={true} duration={500} ><li>หมวดหมู่</li></Link>
                <Link to='ทัวร์ของคุณ' smooth={true} duration={500} ><li>ทัวร์ของคุณ</li></Link>
                <Link to='ประวัติ' smooth={true} duration={500} ><li>ประวัติ</li></Link>
                <Link to='ค้นหา' smooth={true} duration={500} ><li>ค้นหา</li></Link>
            </ul>
            <div className="nav-icons" style={{ display: "flex", alignItems: "center" }}>
                <button className="login-button">เข้าสู่ระบบ/ลงทะเบียน</button>
                <div style={{ marginLeft: "20px" }}>
                    <FaUser className='icons' style={{ fontSize: "2em" }} />
                </div>
            </div>
            <div className="hamburger" onClick={handleNav}>
                {!nav ? <FaBars className='icon' /> : <AiOutlineClose style={{ color: '#000' }} className='icon' />}
            </div>

            <div className={nav ? 'mobile-menu active' : 'mobile-menu'}>
                <ul className="mobile-nav">
                <Link to='หน้าหลัก' smooth={true} duration={500} ><li>หน้าหลัก</li></Link>
                <Link to='หมวดหมู่' smooth={true} duration={500} ><li>หมวดหมู่</li></Link>
                <Link to='ทัวร์ของคุณ' smooth={true} duration={500} ><li>ทัวร์ของคุณ</li></Link>
                <Link to='ประวัติ' smooth={true} duration={500} ><li>ประวัติ</li></Link>
                <Link to='ค้นหา' smooth={true} duration={500} ><li>ค้นหา</li></Link>
                </ul>
                <div className="mobile-menu-bottom">
                    <div className="menu-icons">
                        <button>Account</button>
                    </div>
                    <div className="social-icons">
                        <FaFacebook className='icon' />
                        <FaInstagram className='icon' />
                        <FaTwitter className='icon' />
                        <FaPinterest className='icon' />
                        <FaYoutube className='icon' />
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Navbar
