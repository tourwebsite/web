import React from 'react'
import './HeroStyles.css'
import {AiOutlineSearch} from 'react-icons/ai'

import Image from '../../assets/Phangnga.jfif'

function Hero() {
    return (
        <div className='hero' style={{backgroundImage: `url(${Image})`}}>
            <div className="overlay"></div>
            <div className="content">
                <h1>ค้นหาทัวร์ท่องเที่ยวที่ดีที่สุดในพังงา</h1>
                <form className="form">
                    <div>
                        <input type="text" placeholder='ค้นหาทัวน์ที่คุณต้องการ' />
                    </div>
                    <div>
                        <button><AiOutlineSearch className='icon'/></button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Hero
